import pytest


def format_data_for_display(people):
    ...  # Implement this


def format_data_for_excel(people):
    ...  # Implement this


@pytest.fixture
def example_people_data():
    return [
        {
            'first_name': 'Mary',
            'last_name': 'Sidorenko',
            'title': 'Timlid marketing group'
        },
        {
            'first_name': 'Stepan',
            'last_name': 'Meregko',
            'title': 'Senior Software Engineer'
        }
    ]


def test_format_data_for_display(example_people_data):

    assert format_data_for_display(example_people_data) == [
        'Mary Sidorenko: Timlid marketing group',
        'Stepan Meregko: Senior Software Engineer'
    ]


def test_format_data_fot_excel(example_people_data):

    assert format_data_for_excel(example_people_data) == """Mary,Sidorenko,Timlid marketing group
    Stepan,Meregko,Senior Software Engineer
    """


@pytest.mark.database
def test_db_1():
    pass


@pytest.mark.client
@pytest.mark.database
def test_db_2():
    pass
