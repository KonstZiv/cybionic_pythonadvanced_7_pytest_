from unittest import TestCase


class TryTesting(TestCase):
    def test_always_passes(self):
        self.assertTrue(True)

    def test_always_falls(self):
        self.assertTrue(False)
