import pytest


def is_palindrome(text: str) -> bool:
    ...  # ToDO


def test_is_palindrome_empty_string():
    assert is_palindrome('')


def test_is_palindrome_singl_character():
    assert is_palindrome('a')


def test_is_palindrome_mix_casing():
    assert is_palindrome('Bob')


def test_is_palindrome_with_spases():
    assert is_palindrome('А роза упала на лапу Азора')


def test_is_palindrome_with_punctuation():
    assert is_palindrome('Do geese see God?')


@pytest.mark.parametrize('palindroms', [
    '',
    'a',
    'Bob',
    'А роза упала на лапу Азора',
    'Do geese see God?'
])
def test_is_palindrom():
    assert is_palindrome(palindroms)
