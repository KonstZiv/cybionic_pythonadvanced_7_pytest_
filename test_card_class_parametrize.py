import pytest
from game_v_final import Card


@pytest.mark.parametrize('suit, rank',
                         [('♡', '2'),
                          ('♡', '3'),
                          ('♡', '4'),
                          ('♡', '5'),
                          ('♡', '6'),
                          ('♡', '7'),
                          ('♡', '8'),
                          ('♡', '9'),
                          ('♡', '10'),
                          ('♡', 'J'),
                          ('♡ ', 'Q'),
                          ('♡', 'K'),
                          ('♡', 'T')])
def test_points_cher(suit, rank):
    assert Card(
        suit, rank).points == 1, f'стоимость для карты {suit} {rank} - не единица'

# вынести список задач в переменную
