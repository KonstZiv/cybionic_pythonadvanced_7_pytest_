from game_v_final import Card


def test_points():
    'тестирует правильность работы points'
    assert Card('♠', 'Q').points == 13, 'дама пик - неверная стоимость'
    assert Card('♡', '4').points == 1, 'стоимость червовой масти - не единица'
    assert Card('♣', 'A').points == 0, 'стоимость туза треф отлична от нуля'


# добавить валидацию входных данных в Card с возбуждением ValueError
# with pytest.raises(ValueError) as exinf:
def test_input_validation():
    pass
